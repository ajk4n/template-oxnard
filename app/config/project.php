<?php

return [

	// Product
	'business' => [
		'name' => 'Carpa Canaria',
		'address' => [
			'first_line' => '',
			'second_line' => '',
			'building' => '',
			'city' => '',
			'state' => '',
			'country' => '',
			'zip' => ''
		],
		'id' => '',
		'contact' => [
			'phone' => [
				'primary' => '+34 655 083 665',
				'secondary' => '',
				'absolute' => '0034655083665'
			],
			'email' => [
				'primary' => 'info@carpacanaria.com',
				'secondary' => 'don@dateam.co.za'
			]
		],
		'meta' => [
			'url' => 'http://www.carpacanaria.com',
			'domain' => 'carpacanaria.com',
			'logo' => asset('/assets/global/images/logo.png'),
			'schema' => 'itemscope itemtype="http://schema.org/LocalBusiness"'
		]
	],

	// Development
	'development' => [
		'brand' => 'Theo',
		'email' => [
			'support' => 'support@theohack.com',
			'bug' => 'bugreport@theohack.com'
		],
		'people' => 'Theo M.',
		'url' => 'http://www.theohack.com/'
	],

	// Client
	'client' => [
		'email' => 'info@carpacanaria.com',
		'people' => 'Nicholas and Donald Deeb'
	],

	// Authorisation
	'authorised' => []
];
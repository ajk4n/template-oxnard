<footer>
	<div class="sitemap">
		<div class="container">
			<div class="col-md-4">
				<p title="{{ trans('footer.branding.business.title') }}">
					<img src="{{ Config::get('project.business.meta.logo') }}" alt="{{ trans('footer.branding.business.alt') }}" class="footer-logo">
				</p>
				<p class="branding">{{ trans('footer.branding.advert') }}</p>
			</div>
			<div class="col-md-3">
				<h1>{{ Config::get('project.business.name') }}</h1>
				<ul class="link-list">
					<li title="{{ trans('footer.sitemap.home.title') }}"><a href="#">{{ trans('footer.sitemap.home.value') }}</a></li>
					<li title="{{ trans('footer.sitemap.about.title') }}"><a href="#">{{ trans('footer.sitemap.about.value') }}</a></li>
					<li title="{{ trans('footer.sitemap.gallery.title') }}"><a href="#">{{ trans('footer.sitemap.gallery.value') }}</a></li>
					<li title="{{ trans('footer.sitemap.price.title') }}"><a href="#">{{ trans('footer.sitemap.price.value') }}</a></li>
					<li title="{{ trans('footer.sitemap.contact.title') }}"><a href="#">{{ trans('footer.sitemap.contact.value') }}</a></li>
					<li title="{{ trans('footer.sitemap.fish.title') }}"><a href="#">{{ trans('footer.sitemap.fish.value') }}</a></li>
					<li title="{{ trans('footer.sitemap.funny.title') }}"><a href="#">{{ trans('footer.sitemap.funny.value') }}</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<h1>{{ trans('footer.technical.column_title') }}</h1>
				<ul class="link-list">
					<li title="{{ trans('footer.technical.feedback.title') }}"><a href="#">{{ trans('footer.technical.feedback.value') }}</a></li>
					<li title="{{ trans('footer.technical.status.title') }}"><a href="#">{{ trans('footer.technical.status.value') }}</a></li>
					<li title="{{ trans('footer.technical.press.title') }}"><a href="#">{{ trans('footer.technical.press.value') }}</a></li>
					<li title="{{ trans('footer.technical.graphics.title') }}"><a href="#">{{ trans('footer.technical.graphics.value') }}</a></li>
					<li title="{{ trans('footer.technical.problem.title') }}"><a href="#">{{ trans('footer.technical.problem.value') }}</a></li>
				</ul>
			</div>
			<div class="col-md-2">
				<ul class="link-list media-list">
					<a title="{{ trans('header.social_branding.facebook.title') }}" href="{{ Config::get('social.facebook') }}"><li><i class="fa fa-facebook"></i> Facebook</li></a>
					<a title="{{ trans('header.social_branding.google.title') }}" href="{{ Config::get('social.google') }}"><li><i class="fa fa-google-plus"></i> Google+</li></a>
					<a title="{{ trans('header.social_branding.twitter.title') }}" href="{{ Config::get('social.twitter') }}"><li><i class="fa fa-twitter"></i> Twitter</li></a>
					<a title="{{ trans('header.social_branding.instagram.title') }}" href="{{ Config::get('social.instagram') }}"><li><i class="fa fa-instagram"></i> Instagram</li></a>
					<a title="{{ trans('header.social_branding.vine.title') }}" href="{{ Config::get('social.vine') }}"><li><i class="fa fa-vine"></i> Vine</li></a>
				</ul>
			</div>
		</div>
	</div>
	<div class="tail">
		<div class="container">{{ trans('footer.tail') }}</div>
	</div>
</footer>
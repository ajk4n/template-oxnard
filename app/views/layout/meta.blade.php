	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7, IE=9">
	<meta name="author" content="{{ Config::get('project.development.brand') }}, {{ Config::get('project.client.people') }}">
	<link rel="home" href="{{ Request::root() }}">
	<link rel="index" href="{{ Request::root() }}/sitemap/">
	<link rel="apple-touch-icon" href="{{ Request::root() }}/apple-touch-icon.png">
	<link rel="icon" href="{{ Request::root() }}/favicon.ico">
	<meta property="fb:admins" content="">
	<link rel="stylesheet" href="{{ asset('/assets/global/styles/base.built.css') }}">
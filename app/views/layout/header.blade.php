<header>
	<div class="alert alert-dismissible hidden" role="alert" title="{{ trans('header.alert.title') }}">
		<div class="container">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">{{ trans('header.alert.close') }}</span></button>
			<strong>{{ trans('header.alert.strong') }}</strong> {{ trans('header.alert.message') }}
		</div>
	</div>
	<nav class="navbar">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="sr-only">{{ trans('header.navigation.toggle') }}</span>
				</button>
				<a href="{{ Request::root() }}" class="navbar-brand" title="{{ trans('header.navigation.brand_logo') }}">
					<img src="{{ Config::get('project.business.meta.logo') }}" alt="{{ trans('header.navigation.brand_logo') }}" class="brand-logo">
					<span itemprop="name">{{ Config::get('project.business.name') }}</span>
				</a>
			</div>
			<div class="collapse navbar-collapse" id="navbar">
				<ul class="nav navbar-nav navbar-right">
					<li title="{{ trans('header.navigation.home.title') }}"><a href="#">{{ trans('header.navigation.home.value') }}</a></li>
					<li title="{{ trans('header.navigation.about.title') }}"><a href="#">{{ trans('header.navigation.about.value') }}</a></li>
					<li title="{{ trans('header.navigation.gallery.title') }}"><a href="#">{{ trans('header.navigation.gallery.value') }}</a></li>
					<li title="{{ trans('header.navigation.price.title') }}"><a href="#">{{ trans('header.navigation.price.value') }}</a></li>
					<li title="{{ trans('header.navigation.contact.title') }}"><a href="#">{{ trans('header.navigation.contact.value') }}</a></li>
					<li class="dropdown" id="language">
						<a  href="#" data-toggle="dropdown" class="dropdown-toggle" title="{{ trans('header.navigation.change_language') }}">
							<span>English</span>
							<span class="flag-icon flag-icon-gb"></span>
							<span class="caret"></span>
						</a>
						<ul role="menu" class="dropdown-menu">
							<li><a href="/lang?locale=es" data-id="es" data-language="Español" data-flag="es"><span class="flag-icon flag-icon-es"></span> Español</a></li>
							<li><a href="/lang?locale=de" data-id="de" data-language="Deutsch" data-flag="de"><span class="flag-icon flag-icon-de"></span> Deutsch</a></li>
							<li><a href="/lang?locale=ru" data-id="ru" data-language="русский" data-flag="ru"><span class="flag-icon flag-icon-ru"></span> русский</a></li>
							<li><a href="/lang?locale=se" data-id="se" data-language="Sverige" data-flag="se"><span class="flag-icon flag-icon-se"></span> Sverige</a></li>
							<li><a href="/lang?locale=nl" data-id="nl" data-language="Nederlands" data-flag="nl"><span class="flag-icon flag-icon-nl"></span> Nederlands</a></li>
							<li class="dropdown-header">{{ trans('header.navigation.current_language') }}</li>
							<li><a href="/lang?locale=en" data-id="en" data-language="English" data-flag="gb"><span class="flag-icon flag-icon-gb"></span> English</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="leading-navigation container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="icon-row">
					<a href="{{ Config::get('social.facebook') }}" target="_blank" title="{{ trans('header.social_branding.facebook.title') }}"><i class="fa fa-facebook"></i></a>
					<a href="{{ Config::get('social.google') }}" target="_blank" title="{{ trans('header.social_branding.google.title') }}"><i class="fa fa-google-plus"></i></a>
					<a href="{{ Config::get('social.twitter') }}" target="_blank" title="{{ trans('header.social_branding.twitter.title') }}"><i class="fa fa-twitter"></i></a>
					<a href="{{ Config::get('social.instagram') }}" target="_blank" title="{{ trans('header.social_branding.instagram.title') }}"><i class="fa fa-instagram"></i></a>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="font-increment">
					<button class="btn btn-default"><i class="fa fa-search-plus"></i></button>
					<button class="btn btn-default"><i class="fa fa-search-minus"></i></button>
				</div>
				<div class="contact-information">
					<ul class="list-unstyled">
						<li><a href="#" title="{{ trans('header.contact_information.email.title') }}" itemprop="email">{{ Config::get('project.business.contact.email.primary') }}</a></li>
						<li><a href="tel:{{ Config::get('project.business.contact.phone.absolute') }}" title="{{ trans('header.contact_information.phone.title') }}" itemprop="telephone"><span class="flag-icon flag-icon-es"></span><img src="{{ asset('assets/global/images/brand/whatsapp.png') }}" alt="{{ trans('header.contact_information.custom.whatsapp.alt') }}" title="{{ trans('header.contact_information.custom.whatsapp.title') }}" class="custom-icon">{{ Config::get('project.business.contact.phone.primary') }}</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>
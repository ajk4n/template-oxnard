<!DOCTYPE html>
<html lang="{{ App::getLocale() }}" prefix="og: http://ogp.me/ns#">
<head>
	<title>Document</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="">
	<meta property="og:type" content="">
	<meta property="og:image" content="{{ Config::get('project.business.meta.logo') }}">
	<meta property="og:url" content="{{ Request::url() }}">
	<meta property="og:description" content="">
	<meta property="twitter:card" content="summary">
	<meta property="twitter:url" content="{{ Request::url() }}">
	<meta property="twitter:title" content="">
	<meta property="twitter:description" content="">
	<meta property="twitter:image" content="{{ Config::get('project.business.meta.logo') }}">
	@include('layout.meta')
</head>
<body {{ Config::get('project.business.meta.schema') }}>
	@include('layout.header')
	<article class="billboard">
		<!-- <div class="player" data-property="{videoURL:'UDyIYjoMAiA', containment:'.billboard', autoPlay:true, mute:false, startAt:0, opacity:1, quality:'highres'}"></div> -->
		<div class="container-fluid">
			<div class="col-md-4"></div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="inner-billboard">
					<h1>{{ Config::get('project.business.name') }}</h1>
					<h2>{{ Config::get('project.business.tagline') }}</h2>
					<hr>
					<p>{{ Config::get('project.business.description') }}</p>
				</div>
			</div>
		</div>
	</article>
	@include('layout.footer')
	<script type="text/javascript" src="{{ asset('/assets/global/scripts/core.built.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/assets/global/scripts/player.built.js') }}"></script>
	<script type="text/javascript">
		$(function() {
			$(".player").mb_YTPlayer();
		});
	</script>
</body>
</html>
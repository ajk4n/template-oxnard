<?php

return [

	'alert' => [
		'close' => 'Close',
		'strong' => 'Attention',
		'message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, debitis est nisi. Officia repudiandae perferendis natus unde doloremque explicabo suscipit nihil, pariatur, quasi accusamus tempore, dolorem nobis neque obcaecati, mollitia.',
		'title' => 'This is an announcement'
	],

	"navigation" => [
		'toggle' => 'Toggle Navigation',
		'brand_logo' => 'Logo for ' . Config::get('project.business.name'),
		'home' => [
			'value' => 'Home',
			'title' => 'Catch up on the latest from ' . Config::get('project.business.name')
		],
		'about' => [
			'value' => 'About',
			'title' => 'Learn more about ' . Config::get('project.business.name')
		],
		'gallery' => [
			'value' => 'Gallery',
			'title' => 'Visit the ' . Config::get('project.business.name') . ' Instagram gallery'
		],
		'price' => [
			'value' => 'Pricing',
			'title' => 'Find out about ' . Config::get('project.business.name') . '\'s prices'
		],
		'contact' => [
			'value' => 'Contact',
			'title' => 'Reach out and get in touch with ' . Config::get('project.business.name')
		],
		'current_language' => 'Current Language',
		'change_language' => 'Click here to choose your language from the list'
	],

	'social_branding' => [
		'facebook' => [
			'value' => '',
			'title' => 'Connect with ' . Config::Get('project.business.name') . ' on Facebook'
		],
		'google' => [
			'value' => '',
			'title' => 'Get the latest from ' . Config::Get('project.business.name') . ' on Google+'
		],
		'twitter' => [
			'value' => '',
			'title' => 'Get important news from ' . Config::Get('project.business.name') . ' by following them on Twitter'
		],
		'instagram' => [
			'value' => '',
			'title' => 'Check out ' . Config::Get('project.business.name') . '\'s Instagram gallery'
		],
		'vine' => [
			'value' => '',
			'title' => 'See the best movie clips on Vine from ' . Config::get('project.business.name')
		]
	],

	'contact_information' => [
		'email' => [
			'value' => '',
			'title' => 'Send an email to ' . Config::Get('project.business.name') . ' by clicking this link'
		],
		'phone' => [
			'value' => '',
			'title' => 'Call ' . Config::Get('project.business.name') . ' at your local rate'
		],
		'custom' => [
			'whatsapp' => [
				'value' => '',
				'title' => 'You can reach out to ' . Config::get('project.business.name') . ' by sending them a message on Whatsapp',
				'alt' => 'Whatsapp Logo'
			]
		]
	]

];
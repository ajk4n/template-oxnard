<?php

return [

	'branding' => [
		'business' => [
			'alt' => 'Logo for ' . Config::get('project.business.name'),
			'title' => 'Website and Content is property of ' . Config::get('project.business.name') . ', and it is maintained by ' . Config::get('project.development.brand') . '. ',
		],
		'advert' => 'Proudly hosted with <a target="_blank" href="http://www.digitalocean.com/" title="Built for Developers. Deploy an SSD cloud server in 55 seconds. Free unlimited bandwidth. 3x faster than AWS"><b>DigitalOcean</b></a> and built upon <a target="_blank" href="http://www.laravel.com/" title="Proven Foundation. Laravel is built on top of several Symfony components, giving your application a great foundation of well-tested and reliable code."><b>Laravel</b></a>, crafted with love by <a href="' . Config::get('project.development.url') . '" target="_blank"><b>' . Config::get('project.development.brand') . '</b></a>.',
	],

	'tail' => '&copy;&nbsp;' . Config::get('project.business.name') . ', All Rights Reserved. Website maintained by <a href="' . Config::get('project.development.url') . '">' . Config::get('project.development.brand') . '</a>. Content and Software under <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.',

	'sitemap' => [
		'home' => [
			'value' => 'Homepage',
			'title' => 'Catch up on the latest from ' . Config::get('project.business.name'),
		],
		'about' => [
			'value' => 'About &amp; FAQ',
			'title' => 'Learn more about ' . Config::get('project.business.name'),
		],
		'gallery' => [
			'value' => 'Image Gallery',
			'title' => 'Visit the ' . Config::get('project.business.name') . ' Instagram gallery',
		],
		'price' => [
			'value' => 'Pricing',
			'title' => 'Find out about ' . Config::get('project.business.name') . '\'s prices',
		],
		'contact' => [
			'value' => 'Get in Touch',
			'title' => 'Reach out and get in touch with ' . Config::get('project.business.name'),
		],
		'fish' => [
			'value' => 'About the Fish',
			'title' => 'Learn more about the vast quantity of fish provided by ' . Config::get('project.business.name'),
		],
		'funny' => [
			'value' => 'Strange but True',
			'title' => 'It\'s very strange, but totally true. Here is something which will crack a smile',
		],
	],

	'technical' => [
		'column_title' => 'Help and Information',
		'feedback' => [
			'value' => 'Feedback',
			'title' => 'Have something to say? We want to hear it',
		],
		'status' => [
			'value' => 'Network Status',
			'title' => 'Having a problem with the site? See if the problem is listed here. If you don\'t see it listed, give us a call and let us know',
		],
		'press' => [
			'value' => 'Press and Media',
			'title' => 'Want to feature us somewhere? This is where you\'ll find the material',
		],
		'graphics' => [
			'value' => 'Logos &amp; Badges',
			'title' => 'Download our content and logos here',
		],
		'problem' => [
			'value' => 'Report a Problem',
			'title' => 'If you have experienced a problem with our service, whatever it may be, please report it by clicking this link.',
		],
	],

];
<?php

return [

	'branding' => [
		'business' => [
			'alt' => 'Imagen Asunto por ' . Config::get('project.business.name'),
			'title' => 'Este sitio web y los contenidos almacenados en esta web es propiedad de  ' . Config::get('project.business.name') . ', y se mantiene por ' . Config::get('project.development.brand') . '. ',
		],
		'advert' => 'Orgullosamente acogido con <a target="_blank" href="http://www.digitalocean.com/" title="Construido para Desarrolladores. Implementar un servidor SSD nube en 55 segundos. Ancho de banda ilimitado gratuito. 3x más rápido que AWS"><b>DigitalOcean</b></a> y construido sobre <a target="_blank" href="http://www.laravel.com/" title="Probado Fundación. Laravel se construye en la parte superior de varios componentes de Symfony, dando a su solicitud de una gran base de código bien probado y fiable."><b>Laravel</b></a>, dirigido por <a href="' . Config::get('project.development.url') . '" target="_blank"><b>' . Config::get('project.development.brand') . '</b></a>.',
	],

	'tail' => '&copy;&nbsp;' . Config::get('project.business.name') . ', Todos los derechos reservados. Sitio Web mantenido por <a href="' . Config::get('project.development.url') . '">' . Config::get('project.development.brand') . '</a>. Contenido y Software bajo licencia <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.',

	'sitemap' => [
		'home' => [
			'value' => 'Página Principal',
			'title' => 'Obtenga la información más reciente acerca de ' . Config::get('project.business.name'),
		],
		'about' => [
			'value' => 'Acerca y Preguntas Frecuentes',
			'title' => 'Más información sobre ' . Config::get('project.business.name'),
		],
		'gallery' => [
			'value' => 'Galería de Imágenes',
			'title' => 'Ir a la galería ' . Config::get('project.business.name'),
		],
		'price' => [
			'value' => 'Precios',
			'title' => 'Información sobre la fijación de precios para ' . Config::get('project.business.name'),
		],
		'contact' => [
			'value' => 'Contacto',
			'title' => 'Póngase en contacto con ' . Config::get('project.business.name'),
		],
		'fish' => [
			'value' => 'Sobre el Pescado',
			'title' => 'Más información sobre la gran cantidad de peces en ' . Config::get('project.business.name'),
		],
		'funny' => [
			'value' => 'Extraño pero Cierto',
			'title' => 'Películas divertidas y fotos de' . Config::get('project.business.name'),
		],
	],

	'technical' => [
		'column_title' => 'Ayuda e Información',
		'feedback' => [
			'value' => 'Realimentación',
			'title' => 'Si tiene algo que decir? Queremos escucharlo',
		],
		'status' => [
			'value' => 'Estado de la Red',
			'title' => '¿Tiene algún problema con el sitio? A ver si el problema está aquí. Si no aparece en la lista, llámenos y háganos saber',
		],
		'press' => [
			'value' => 'Periodismo y Medios de Comunicación',
			'title' => '¿Quieres que nos cuentan en alguna parte? Aquí es donde usted encontrará el material',
		],
		'graphics' => [
			'value' => 'Gráficos de Asunto',
			'title' => 'Descargue nuestro contenido y logotipos aquí',
		],
		'problem' => [
			'value' => 'Informar Sobre un Problema',
			'title' => 'Si usted ha sufrido un problema con nuestro servicio, sea lo que sea, por favor repórtelo haciendo clic en este enlace.',
		],
	],

];
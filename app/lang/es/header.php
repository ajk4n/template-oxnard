<?php

return [

	'alert' => [
		'close' => 'Cerca',
		'strong' => 'Atención',
		'message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, debitis est nisi. Officia repudiandae perferendis natus unde doloremque explicabo suscipit nihil, pariatur, quasi accusamus tempore, dolorem nobis neque obcaecati, mollitia.',
		'title' => 'Este es un anuncio',
	],

	"navigation" => [
		'toggle' => 'Alternar Navegación',
		'brand_logo' => 'Imagen Asunto por ' . Config::get('project.business.name'),
		'home' => [
			'value' => 'Inicio',
			'title' => 'Obtenga la información más reciente acerca de ' . Config::get('project.business.name'),
		],
		'about' => [
			'value' => 'Información',
			'title' => 'Más información sobre ' . Config::get('project.business.name'),
		],
		'gallery' => [
			'value' => 'Galería',
			'title' => 'Ir a la galería ' . Config::get('project.business.name'),
		],
		'price' => [
			'value' => 'Precios',
			'title' => 'Información sobre la fijación de precios para ' . Config::get('project.business.name'),
		],
		'contact' => [
			'value' => 'Contacto',
			'title' => 'Póngase en contacto con ' . Config::get('project.business.name'),
		],
		'current_language' => 'Idioma actual',
		'change_language' => 'Haga clic aquí para seleccionar su idioma de la lista',
	],

	'social_branding' => [
		'facebook' => [
			'value' => '',
			'title' => 'Conectar con ' . Config::Get('project.business.name') . ' en Facebook',
		],
		'google' => [
			'value' => '',
			'title' => 'Recibe las noticias más recientes de ' . Config::Get('project.business.name') . ' en Google+',
		],
		'twitter' => [
			'value' => '',
			'title' => 'Recibe las últimas noticias desde ' . Config::Get('project.business.name') . ' en pos de ellas en Twitter',
		],
		'instagram' => [
			'value' => '',
			'title' => 'Ver las más recientes imágenes de la ' . Config::Get('project.business.name') . 'en su galería Instagram',
		],
		'vine' => [
			'value' => '',
			'title' => 'Vea fragmentos de películas cortas de ' . Config::get('project.business.name') . ' en Vine',
		],
	],

	'contact_information' => [
		'email' => [
			'value' => '',
			'title' => 'Enviar un correo electrónico a ' . Config::Get('project.business.name') . ' haciendo clic en este enlace',
		],
		'phone' => [
			'value' => '',
			'title' => 'Llame ' . Config::Get('project.business.name') . ' a su tarifa local',
		],
		'custom' => [
			'whatsapp' => [
				'value' => '',
				'title' => 'Enviar ' . Config::get('project.business.name') . ' un mensaje en Whatsapp',
				'alt' => 'logotipo de la empresa',
			],
		],
	],

];
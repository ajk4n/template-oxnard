var makeTranslation = function() {
	sessionLanguage = $('html').attr('lang'); // store the value of the language attribute
	languageOption = $('#language > a').children().first(); // store name of default language in dropdown menu

	currentLanguage = {
		name: languageOption,
		flag: languageOption.next()
	}

	chosenLanguage = $('header').find('[data-id="' + sessionLanguage + '"]'); // match the new language based on `sessionLanguage`
	currentLanguage.name.text(chosenLanguage.data('language')); // change the language text
	currentLanguage.flag.removeClass('flag-icon-gb').addClass('flag-icon-' + chosenLanguage.data('flag')); // change the language flag

	chosenLanguageElement = chosenLanguage.parent(); // get the parent `li`
	currentLanguageElement = $('#language > .dropdown-menu').children().last(); // get the 'current language' `li` element

	chosenLanguageElement.replaceWith(currentLanguageElement); // replace the selected language with the current language

	$('#language > .dropdown-menu').append(chosenLanguageElement);
}

makeTranslation();